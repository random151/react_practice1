import './App.css';

import Card from './components/Card';

function App() {
  return (
    <>
      <h1>Hello, React!</h1>
      <Card />
    </>
  );
}

export default App;